FROM docker.elastic.co/elasticsearch/elasticsearch-oss:6.2.2
COPY elasticsearch-cn.yml /elasticsearch-cn.yml
COPY elasticsearch-es1.yml /elasticsearch-es1.yml
COPY elasticsearch-es2.yml /elasticsearch-es2.yml
USER root
RUN mkdir -p /var/es_snapshots
RUN chown elasticsearch:elasticsearch /usr/share/elasticsearch/data
RUN bin/elasticsearch-plugin install -b com.floragunn:search-guard-6:6.2.2-21.0 
RUN chmod -R 777 /usr/share/elasticsearch
COPY keys/* /usr/share/elasticsearch/config/
COPY keys/*.jks /usr/share/elasticsearch/plugins/search-guard-6/tools/
COPY sg_roles.yml /usr/share/elasticsearch/plugins/search-guard-6/sgconfig/
COPY sg_internal_users.yml /usr/share/elasticsearch/plugins/search-guard-6/sgconfig/
COPY start.sh /start.sh
ENTRYPOINT ["/start.sh"]
CMD ["elasticsearch"]
