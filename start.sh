#!/bin/sh
set -eu
echo Checking elasticsearch setup...
mapmax=`cat /proc/sys/vm/max_map_count`
filemax=`cat /proc/sys/fs/file-max`
filedescriptors=`ulimit -n`
echo "fs.file_max: $filemax"
echo "vm.max_map_count: $mapmax"
echo "ulimit.file_descriptors: $filedescriptors"
fds=`ulimit -n`
if [ "$fds" -lt "64000" ] ; then
  echo ""
  echo "Elasticsearch recommends 64k open files per process. you have $filedescriptors"
  echo "the docker deamon should be run with increased file descriptors to increase those available in the container"
  echo " try \`ulimit -n 64000\`"
else
  echo "you have more than 64k allowed file descriptors. awesome"
fi
if [ "$1" = 'elasticsearch' ]; then
  echo -e '\nStarting elasticsearch...'
fi
if  [[ $(hostname -s) = cn-* ]]; then 
cp /elasticsearch-cn.yml /usr/share/elasticsearch/config/elasticsearch.yml 
elif  [[ $(hostname -s) = es-1-* ]]; then 
cp /elasticsearch-es1.yml /usr/share/elasticsearch/config/elasticsearch.yml 
else
cp /elasticsearch-es2.yml /usr/share/elasticsearch/config/elasticsearch.yml
fi

#cd /usr/share/elasticsearch/plugins/search-guard-6/tools
#./sgadmin.sh -ts truststore.jks -tspass 28e9f01d24ae6f3933cc -ks CN=sgadmin-keystore.jks -kspass 2b190fb510e9c673fa2e -nhnv -icl -cd ../sgconfig/
#oc create configmap es-es-2 --from-file=/home/ubuntu/sg_users/es-2
#oc volume dc/es-2 --add --name=es-es-2  -m /usr/share/elasticsearch/config -t configmap --configmap-name=es-es-2
exec "$@"
